#ifndef _COLOR
#define _COLOR

#include <ncurses.h>
#include <string>
#include <map>

struct Color
{
	int index;
	string name;
	short fg, bg;
}

class ColorManager
{
	public:
	ColorManager();
	void AddColor( const string& name, int index, short fg, short bg );
	int GetColor( const string& name );

	private:
	string GetColorName( short color );

	map< string, Color > colors;
};

ColorManager::ColorManager()
{
	// Setup all the colors
}

void ColorManager::AddColor( const string& name, int index, short fg, short bg )
{
	Color newColor;
	newColor.index = index;
	newColor.name = name;
	newColor.fg = fg;
	newColor.bg = bg;
	init_pair( index, fg, bg );
	colors.insert( pair< string, Color >( name, newColor ) );
}

int ColorManager::GetColor( const string& name )
{
	return colors[ name ];
}

string ColorManager::GetColorName( short color )
{
	if 	( color == 0 ) { return "black"; }
	else if ( color == 1 ) { return "red"; }
	else if ( color == 2 ) { return "green"; }
	else if ( color == 3 ) { return "yellow"; }
	else if ( color == 4 ) { return "blue"; }
	else if ( color == 5 ) { return "magenta"; }
	else if ( color == 6 ) { return "cyan"; }
	else if ( color == 7 ) { return "white"; }
	return "NOIDEA";
}

#endif
