#include <ncurses.h>
#include <iostream>
using namespace std;

int main()
{
	initscr();
	raw();
	keypad( stdscr, TRUE );
	noecho();
	start_color();

	init_pair( 1, COLOR_BLACK, COLOR_BLUE );	// Walls
	init_pair( 2, COLOR_WHITE, COLOR_CYAN );	// Floors
	init_pair( 3, COLOR_RED, COLOR_MAGENTA );

	for ( int y = 0; y < 25; y++ )
	{
		for ( int x = 0; x < 80; x++ )
		{
			if ( y == 0 || y == 24 || x == 0 || x == 79 )
			{
				attron( COLOR_PAIR( 1 ) );
				mvaddch( y, x, 'W' );
				attroff( COLOR_PAIR( 1 ) );
			}
			else
			{
				attron( COLOR_PAIR( 2 ) );
				mvaddch( y, x, 'F' );
				attroff( COLOR_PAIR( 2 ) );
			}
		}
	}

	attron( COLOR_PAIR( 3 ) );
	mvaddch( 5, 5, 'X' );
	attroff( COLOR_PAIR( 3 ) );


	refresh();

	int input = getch();

	endwin();

	return 0;
}
