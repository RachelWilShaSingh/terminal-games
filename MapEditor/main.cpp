#include <ncurses.h>
#include <string>
#include <map>

#include "Color.hpp"
#include "Util.hpp"
#include "Logger.hpp"

const int mapWidth = 80;
const int mapHeight = 20;
const int screenWidth = 80;
const int screenHeight = 24;

int cursorX=0, cursorY=0;
int curColor;
std::string foreground;
std::string background;
std::string curScreen = "main";

int board[ mapWidth ][ mapHeight ];

std::map<std::string, Color> COLOR;

void SetupColors();
void DrawGui();
void DrawBoard();
void FillBoard( short color );
std::string GetColorName( short color );

int main()
{
    Logger::Init();
	initscr();
	keypad( stdscr, TRUE );
	if ( !has_colors() )
	{
        return 84;
	}
    start_color();
    SetupColors();

    bool done = false;
    foreground = "red";
    background = "yellow";
    curColor = COLOR[ foreground + "-" + background ].index;
    Logger::Log( "" );

    while ( !done )
    {
        clear();
        DrawGui();
        DrawBoard();
        move( cursorY, cursorX );

        // input
        int keypress = getch();

        if ( keypress == KEY_LEFT ) { cursorX--; }
        else if ( keypress == KEY_RIGHT ) { cursorX++; }
        else if ( keypress == KEY_UP ) { cursorY--; }
        else if ( keypress == KEY_DOWN ) { cursorY++; }

        // Place
        else if ( keypress == KEY_F(1) )
        {
            board[ cursorX ][ cursorY ] = curColor;
        }

        // Clear
        else if ( keypress == KEY_F(2) )
        {
            board[ cursorX ][ cursorY ] = COLOR[ "black-black" ].index;
        }

        // Foreground
        else if ( keypress == KEY_F( 3 ) )
        {
            curScreen = "foreground";
        }

        // Background
        else if ( keypress == KEY_F( 4 ) )
        {
            curScreen = "background";
        }

        // Symbol
        else if ( keypress == KEY_F( 5 ) )
        {
        }

        // Fill
        else if ( keypress == KEY_F( 6 ) )
        {
            curScreen = "fill";
        }

        // Save
        else if ( keypress == KEY_F( 10 ) )
        {
        }

        // Quit
        else if ( keypress == KEY_F( 12 ) )
        {
            done = true;
        }

        else if ( keypress >= 49 || keypress <= 57 )
        {
            int number = keypress - 48;
            if ( curScreen == "foreground" )
            {
                foreground = GetColorName( number );
            }
            else if ( curScreen == "background" )
            {
                background = GetColorName( number );
            }
            else if ( curScreen == "fill" )
            {
                FillBoard( COLOR[ "white-" + GetColorName( number ) ].index );
            }
            Logger::Log( "Color selected: " + Util::IntToString( number ) );
            Logger::Log( "New colors: " + foreground + "-" + background );
            curColor = COLOR[ foreground + "-" + background ].index;
            curScreen = "main";
        }
    }

    refresh();
	getch();
	endwin();
    Logger::Close();
    return 0;
}

void SetupColors()
{
    int ct = 0;
    for ( int bg = 0; bg < 8; bg++ )
    {
        for ( int fg = 0; fg < 8; fg++ )
        {
            std::string name = GetColorName( fg ) + "-" + GetColorName( bg );
            Color newColor;
            newColor.Setup( name, ct, fg, bg );
            COLOR.insert( std::pair< std::string, Color >( name, newColor ) );

            Logger::Log( "\nAdd color " + Util::IntToString( fg ) + "-" + Util::IntToString( bg ) );
            Logger::Log( "\tName: " + name );
            Logger::Log( "\tIndex: " + Util::IntToString( ct ) );
            ct++;
        }
    }
}

void DrawBoard()
{
    for ( int y = 0; y < mapHeight; y++ )
    {
        for ( int x = 0; x < mapWidth; x++ )
        {
            attron( COLOR_PAIR( board[x][y] ) );
            mvprintw( y, x, " " );
            attroff( COLOR_PAIR( board[x][y] ) );
        }
    }
}

void FillBoard( short color )
{
    for ( int y = 0; y < mapHeight; y++ )
    {
        for ( int x = 0; x < mapWidth; x++ )
        {
            board[x][y] = color;
        }
    }
}

void DrawGui()
{
    if ( curScreen == "main" )
    {
        attron( COLOR_PAIR( COLOR["white-black"].index ) );
        attron( A_BOLD );
        mvprintw( 22, 0, "(F3): Foreground  (F4): Background    (F5): Symbol    (F6): Fill" );
        mvprintw( 23, 0, "(F1): Place       (F2): Clear         (F10): Save     (F12): Quit" );
        attroff( A_BOLD );
        attroff( COLOR_PAIR( COLOR["white-black"].index ) );
    }
    else if ( curScreen == "foreground" || curScreen == "background" || curScreen == "fill" )
    {
        attron( COLOR_PAIR( COLOR["white-black"].index ) );
        attron( A_BOLD );
        mvprintw( 21, 0, "COLORS:" );
        mvprintw( 20, 0, curScreen.c_str() );
        attroff( A_BOLD );

        int y = 22;
        int x = 0;
        int div = screenWidth / 4;
        for ( int i = 0; i < 8; i++ )
        {
            std::string text = Util::IntToString( i ) + ": " + GetColorName( i );
            mvprintw( y, x * div, text.c_str() );
            if ( ++x == 4 ) { y++; x = 0; }
        }

        attroff( COLOR_PAIR( COLOR["white-black"].index ) );
    }
}

std::string GetColorName( short color )
{
    switch( color )
    {
        case 0:
            return "black";
        break;

        case 1:
            return "red";
        break;

        case 2:
            return "green";
        break;

        case 3:
            return "yellow";
        break;

        case 4:
            return "blue";
        break;

        case 5:
            return "magenta";
        break;

        case 6:
            return "cyan";
        break;

        case 7:
            return "white";
        break;

        default:
            return "none";
        break;
    }
}
