#ifndef _LOGGER
#define _LOGGER

#include <string>
#include <fstream>

class Logger
{
    public:
    static void Init();

    static void Log( const std::string& text );

    static void Close();

    private:
    static std::ofstream m_out;
};

#endif
