#include "Character.hpp"
#include "Util.hpp"

#include <ncurses.h>
#include <stdlib.h>

Character::Character()
{
    m_step = 0;
    m_score = 0;
}

void Character::Setup( int x, int y, char symbol, int color )
{
    ClearSprite();
    SetPosition( x, y );
    m_symbol[1][1] = symbol;
    m_color = color;
}

void Character::Setup( char symbol, int color )
{
    ClearSprite();
    m_symbol[1][1] = symbol;
    m_color = color;
}

void Character::Setup( CharacterType type, int color )
{
    m_color = color;
    ClearSprite();
    if ( type == SPRITE_HUMAN )
    {
        /*
         O
        /|\
        / \
        */
        m_symbol[1][0] =  'O';
        m_symbol[0][1] = '/';
        m_symbol[1][1] = '|';
        m_symbol[2][1] = '\\';
        m_symbol[0][2] = '/';
        m_symbol[2][2] = '\\';
    }
    else if ( type == SPRITE_TREASURE )
    {
        /*
        ===
        ===
        */
        m_symbol[1][0] = '=';
        m_symbol[0][1] = '=';
        m_symbol[1][1] = '=';
        m_symbol[2][1] = '=';
        m_symbol[0][2] = '=';
        m_symbol[1][2] = '=';
        m_symbol[2][2] = '=';
    }
}

void Character::ClearSprite()
{
    for ( int y = 0; y < 3; y++ )
    {
        for ( int x = 0; x < 3; x++ )
        {
            m_symbol[x][y] = ' ';
        }
    }
}

void Character::SetPosition( int x, int y )
{
    m_x = x;
    m_y = y;
}

void Character::SetRandomPosition( int minX, int minY, int maxX, int maxY )
{
    m_x = minX + ( rand() % ( maxX - minX + 1 ) );
    m_y = minY + ( rand() % ( maxY - minY + 1 ) );
}

void Character::Draw()
{
    // Temporary adjustment of previous drawing
    attron( COLOR_PAIR( m_color ) );
    for ( int y = 0; y < 3; y++ )
    {
        for ( int x = 0; x < 3; x++ )
        {
            const char temp[2] = { m_symbol[x][y], '\0' };
            mvprintw( m_y+y, m_x+x, temp );
        }
    }
    attroff( COLOR_PAIR( m_color ) );
}

int Character::GetX()
{
    return m_x;
}

int Character::GetY()
{
    return m_y;
}

void Character::HandleKeypress( int k, int minX, int minY, int maxX, int maxY )
{
    if ( k == KEY_LEFT && m_x - 1 > minX )
    {
        m_x--;
        m_step++;
    }
    else if ( k == KEY_RIGHT && m_x + 1 < maxX )
    {
        m_x++;
        m_step++;
    }
    else if ( k == KEY_UP && m_y - 1 > minY )
    {
        m_y--;
        m_step++;
    }
    else if ( k == KEY_DOWN && m_y + 1 < maxY )
    {
        m_y++;
        m_step++;
    }
}

void Character::AddToScore()
{
    m_score++;
}

int Character::GetScore()
{
    return m_score;
}
