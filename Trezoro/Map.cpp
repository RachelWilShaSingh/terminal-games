#include "Map.hpp"

#include <ncurses.h>
#include <stdlib.h>

void Map::Generate( int width, int height )
{
    m_mapWidth = width;
    m_mapHeight = height;

    for ( int y = 0; y < height; y++ )
    {
        for ( int x = 0; x < width; x++ )
        {
            Tile newTile;
            newTile.x = x;
            newTile.y = y;

            GetTileSymbol( newTile, x, y, width, height );

            m_lstTiles.push_back( newTile );
        }
    }
}

void Map::GetTileSymbol( Tile& tile, int x, int y, int width, int height )
{
    if ( y == 0 || y == height-1 || x == 0 || x == width-1 )
    {
        tile.symbol = '#';
        tile.color = FENCE;
    }
    else
    {
        int randTile = rand() % 20;

        if ( randTile > 2 )
        {
            tile.symbol = ' ';
            tile.color = GRASS;
        }
        else if ( randTile == 0 )
        {
            tile.symbol = '*';
            tile.color = FLOWER;
        }
        else if ( randTile == 1 )
        {
            tile.symbol = 'v';
            tile.color = GRASS;
        }
        else if ( randTile == 2 )
        {
            tile.symbol = '@';
            tile.color = WATER;
        }
    }
}

void Map::Draw()
{
    for ( unsigned int i = 0; i < m_lstTiles.size(); i++ )
    {
        const char temp[2] = { m_lstTiles[i].symbol, '\0' };
        attron( COLOR_PAIR( m_lstTiles[i].color ) );
        mvprintw( m_lstTiles[i].y, m_lstTiles[i].x, temp );
        attroff( COLOR_PAIR( m_lstTiles[i].color ) );
    }
}
