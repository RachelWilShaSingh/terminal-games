#ifndef _MAP
#define _MAP

#include <vector>

enum TileTypes { GRASS=10, FENCE=11, WATER=12, FLOWER=13, EMPTY=14, HUMAN=15, TREASURE=16, HUD=17 };

class Tile
{
    public:
    int x, y;
    char symbol;
    int color;
};

class Map
{
    public:
    void Generate( int width, int height );
    void Draw();

    private:
    void GetTileSymbol( Tile& tile, int x, int y, int width, int height );
    int m_mapWidth, m_mapHeight;
    std::vector<Tile> m_lstTiles;
};

#endif
